"1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
    <xsl:output method="text"  />
    <xsl:template match="/">

        <xsl:variable name="labels" >
            <dict>
                <clinical_data>
					<family_history>
						<Positive>obciążony</Positive>
						<Negative>nieobciążony</Negative>
						<Unknown>brak danych</Unknown>
					</family_history>
				</clinical_data>
				<assesment>
					<location_prostate>
						<BASE>przypodstawnej</BASE>
						<MID>środkowej</MID>
						<APEX>przyszczytowej</APEX>
						<LEFT>lewego płata</LEFT>
						<RIGHT>prawego płata</RIGHT>
					</location_prostate>
							<location_sv>translate(.,',','.'),
						<LEFT>lewym</LEFT>
						<RIGHT>prawym</RIGHT>
					</location_sv>
				</assesment>
				<staging>
					<TNM>
						<TX>nie ma możliwości oceny guza pierwotnego</TX>
						<T0>nie ma dowodów na istnienie guza pierwotnego</T0>
						<T1>guz klinicznie niejawny, którego nie stwierdza się w badaniu klinicznym i badaniach obrazowych</T1>
						<T1a>guz wykryty przypadkowo na podstawie badania histopatologicznego w niewięcej niż 5% wyciętej tkanki gruczołu krokowego</T1a>
						<T1b>guz wykryty przypadkowo na podstawie badania histopatologicznego w ponad 5% wyciętej tkanki gruczołu krokowego</T1b>
						<T1c>guz wykryty na podstawiawie badania histopatologicznego materiału uzyskanego z biopsji stercza (TRUS B) wykonanej z powodu podwyższonego poziomu PSA</T1c>
						<T2>guz ograniczony do gruczołu krokowego</T2>
						<T2a>guz zajmuje mniej niż połowę jednego płata gruczołu krokowego</T2a>
						<T2b>guz zajmuje więcej niż połowę jednego płata gruczołu krokowego, ale nie zajmuje drugiego płata</T2b>
						<T2c>guz lokalizowany jest w obu płatach gruczołu krokowego</T2c>
						<T3>guz nacieka poza torebkę stercza</T3>
						<T3a>guz nacieka poza torebkę po jednej lub obu stronach stercza</T3a>
						<T3b>guz nacieka pęcherzyki nasienne po jednej lub obu stronach</T3b>
						<T4>nieruchomy guz naciekający na struktury otaczające inne niż pęcherzyki nasienne - szyja pęcherza moczowego, zwieracz zewnętrzny, odbytnicę, mięsień dźwigacz odbytnicy, ścianę miednicy mniejszej</T4>
						<NX>nie ma możliwości oceny węzłów chłonnych</NX>
						<N0>brak przerzutów do węzłów chłonnych</N0>
						<N1>przerzuty w regionalnych węzłach chłonnych</N1>
						<M0>nie ma przerzutów odległych</M0>
						<M1>istnieją przerzuty odległe</M1>
						<M1a>istnieją przerzuty do węzłów chłonnych innych niż regionalne</M1a>
						<M1b>istnieją przerzuty do kości</M1b>
						<M1c>istnieją przerzuty o innym umiejscowieniu niż do kości/węzłów chłonnych innych niż regionalne</M1c>
					</TNM>
				</staging>
			</dict>
    </xsl:variable>
	
	<xsl:variable name="if_referral" select="string-length(//referral_diagnosis) > 0" > </xsl:variable>
	<xsl:variable name="if_psa" select="string(//morphology_psa_value) !='X' and string-length(//morphology_psa_value) > 0" />
	<xsl:variable name="if_psa_value_aquisition_date" select="string-length(//morphology_psa_value_aquisition_date) > 0" />
	<xsl:variable name="if_family_history" select="string-length(//family_history) > 0" />
	<xsl:variable name="if_family_history_not_unknown" select="string(//family_history) !='Unknown'" />
		<xsl:variable name="family_hist" select=".//family_history" />
		<xsl:variable name="family_history_value" select="$labels/.//family_history/*[name()= $family_hist]"/>
	<xsl:variable name="if_assesment_or_staging_without_lession_correspond_histopatology" select="//examination_type = ('ASSESMENT') or (//examination_type = ('STAGING') and ( //lesion_correspond_histopathology ='No' or //lesion_correspond_histopathology ='no-information'))" />	
		
	<xsl:variable name="if_staging_with_lession_correspond_histopatology" select="(//examination_type = ('STAGING')) and (//lesion_correspond_histopathology = ('Yes'))" />	
	<xsl:variable name="if_Assesment" select="(//examination_type = 'ASSESMENT')" />
	<xsl:variable name="if_Staging" select="(//examination_type = 'STAGING')" />
	<xsl:variable name="if_Assesment_or_Staging" select="(//examination_type = ('ASSESMENT','STAGING'))" />
	<xsl:variable name="if_Reccurence" select="//examination_type = 'RECCURENCE'" />
	<xsl:variable name="if_spectroscopy_performed" select="(//examination_type = 'ASSESMENT') and (//report_spectroscopy_performed = 'true')" />
	<xsl:variable name="if_type_of_therapy" select="string-length(//type_of_therapy) > 0" />
	<xsl:variable name="if_not_RP" select="not(tokenize(//type_of_therapy,' ') ='RP') " />
	<xsl:variable name="if_RP" select="(tokenize(//type_of_therapy,' ') ='RP') " />
	<xsl:variable name="if_TURP" select="(tokenize(//type_of_therapy,' ') ='TURP') " />
	<xsl:variable name="if_TURP_and_not_RP" select="((tokenize(//type_of_therapy,' ') ='TURP') and not(tokenize(//type_of_therapy,' ') ='RP')) " />
	<xsl:variable name="hormonal_therapy" select="(tokenize(//type_of_therapy,' ') ='hormonal_therapy') " />
	<xsl:variable name="radiotherapy" select="(tokenize(//type_of_therapy,' ') ='radiotherapy') " />
	<xsl:variable name="focal_therapy" select="(tokenize(//type_of_therapy,' ') ='focal_therapy') " />
	<xsl:variable name="other" select="(tokenize(//type_of_therapy,' ') ='other') " />
	<xsl:variable name="if_Anti_BPHtherapy" select="string-length(//clinical_data_anti_BPH_therapy) > 0" />
	<xsl:variable name="if_finasteride" select="(tokenize(//clinical_data_anti_BPH_therapy,' ') ='finasteride') " />
	<xsl:variable name="if_Prostate_dimensions" select="(string-length(//prostate_dim_l) > 0) and (string-length(//prostate_dim_w) > 0) and (string-length(//prostate_dim_h) > 0) and (string-length(//prostate_volume_calculated) > 0)" />
	<xsl:variable name="if_psa" select="(//morphology_psa = 'Yes') and (not(tokenize(//morphology_psa_value,' ') ='X') and (string-length(//morphology_psa_value) > 0) )" />
	<xsl:variable name="if_lesion_PZ_TZ_CZ_AS" select="(//lesion_location_actual = ('PZ','TZ','CZ','AS'))" />
	<xsl:variable name="if_lesion_urethra" select="(//lesion_location_actual = 'U')" />
	<xsl:variable name="if_lesion_seminal_vesicle" select="(//lesion_location_actual = 'SV')" />
		<xsl:variable name="location_horiz_sv" select=".//lesion_location_horizontal" />
		<xsl:variable name="location_horiz_sv_label" select="$labels/.//location_sv/*[name()= $location_horiz_sv]"/>
	<xsl:variable name="if_lesion_dimensions" select="(string-length(//lesion_dim_short) > 0) and (string-length(//lesion_dim_long) > 0) and (string-length(//lesion_dim_vertical) > 0) and (string-length(//lesion_dim_max) > 0)" />
	<xsl:variable name="if_pirads_manual" select="((//pirads_manual = ('1','2','3','4','5')) and (//lesion_location_actual = ('PZ','TZ','AS')))" />
	<xsl:variable name="if_tnm_t_value" select="string-length(//staging_tnm_t) > 0" />
		<xsl:variable name="tnm_t_v" select=".//staging_tnm_t" />
		<xsl:variable name="tnm_t" select="$labels/.//TNM/*[name()= $tnm_t_v]"/>
	<xsl:variable name="if_tnm_n_value" select="string-length(//staging_tnm_n) > 0" />
		<xsl:variable name="tnm_n_v" select=".//staging_tnm_n" />
		<xsl:variable name="tnm_n" select="$labels/.//TNM/*[name()= $tnm_n_v]"/>
	<xsl:variable name="if_tnm_m_value" select="string-length(//staging_tnm_m) > 0" />
		<xsl:variable name="tnm_m_v" select=".//staging_tnm_m" />
		<xsl:variable name="tnm_m" select="$labels/.//TNM/*[name()= $tnm_m_v]"/>
	
	<xsl:variable name="if_clinical_data_buscolysin" select="//clinical_data_buscolysin = 'true'" />	
	<xsl:variable name="if_clinical_data_erc" select="//clinical_data_erc = 'Yes'" />
		
	<xsl:variable name="if_Bladder_ROI_value" select="string(//Bladder_ROI_value) !='X' and string-length(//Bladder_ROI_value) > 0" />	
	<xsl:variable name="if_Salt_ROI_value" select="string(//Salt_ROI_value) !='X' and string-length(//Salt_ROI_value) > 0" />	
	<xsl:variable name="if_t2w_lesion_ROI" select="string(//t2w_lesion_ROI) !='X' and string-length(//t2w_lesion_ROI) > 0" />	
	<xsl:variable name="if_t2w_ratio_bladder_ROI" select="string(//t2w_ratio_bladder_ROI) !='X' and string-length(//t2w_ratio_bladder_ROI) > 0" />	
	<xsl:variable name="if_t2w_ratio_salt_ROI" select="string(//t2w_ratio_salt_ROI) !='X' and string-length(//t2w_ratio_salt_ROI) > 0" />	
	<xsl:variable name="if_adc_lesion_ROI" select="string(//adc_lesion_ROI) !='X' and string-length(//adc_lesion_ROI) > 0" />	
	<xsl:variable name="if_adc_ratio_bladder_ROI" select="string(//adc_ratio_bladder_ROI) !='X' and string-length(//adc_ratio_bladder_ROI) > 0" />	
	<xsl:variable name="if_adc_ratio_salt_ROI" select="string(//adc_ratio_salt_ROI) !='X' and string-length(//adc_ratio_salt_ROI) > 0" />	
	<xsl:variable name="if_DWI_lesion_ROI" select="string(//DWI_lesion_ROI) !='X' and string-length(//DWI_lesion_ROI) > 0" />	
	<xsl:variable name="if_DWI_ratio_bladder_ROI" select="string(//DWI_ratio_bladder_ROI) !='X' and string-length(//DWI_ratio_bladder_ROI) > 0" />	
	<xsl:variable name="if_DWI_ratio_salt_ROI" select="string(//DWI_ratio_salt_ROI) !='X' and string-length(//DWI_ratio_salt_ROI) > 0" />	
	<xsl:variable name="if_DCE_lesion_ROI" select="string(//DCE_lesion_ROI) !='X' and string-length(//DCE_lesion_ROI) > 0" />	
	<xsl:variable name="if_DCE_ratio_bladder_ROI" select="string(//DCE_ratio_bladder_ROI) !='X' and string-length(//DCE_ratio_bladder_ROI) > 0" />	
	<xsl:variable name="if_DCE_ratio_salt_ROI" select="string(//DCE_ratio_salt_ROI) !='X' and string-length(//DCE_ratio_salt_ROI) > 0" />	
		
<xsl:if test="$if_referral">W skierowaniu: <xsl:value-of select="//referral_diagnosis"/><xsl:text>